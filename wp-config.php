<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

//define( 'WP_DEBUG', true );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-restaurantvandenberg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xMjG[cT~Capq1~D%u_h2vMt_tix1&mDz1gtr +mXr@F&bCsT_TLV-L}iJfGUXlZE');
define('SECURE_AUTH_KEY',  'On?vOtcC]!:^WF]qhm_(S9,^OhbQ7ifGZke6]l2KJ;RL!4z($l3+(]p}[N;i_Uq)');
define('LOGGED_IN_KEY',    '//)6HUO8NsBU;^5XGP-BkjO]SvK]&YwHD~GM,`e?Or4kK046ZSV[uutNX{WG#~jM');
define('NONCE_KEY',        '04OXi1mIg+!Ny-)J C@(P= e;Hd|Wl+wQVq|djBFU[==0/gQ=N9-3D0Mz{*O.W44');
define('AUTH_SALT',        '.@b#8$=e=-Tk^hN|S>7IIt7,Hq/k$K*.kiG|/OZGkip$]`|U^+:pDmft+@Js-$c_');
define('SECURE_AUTH_SALT', 'M2NAl,mN/mov65(U?>?FoPsjRk>F-%fN5I`1+k*4nb,{F0<&@5^cH*QXx=Y+8E]9');
define('LOGGED_IN_SALT',   'XVx}?Uk_[-J(Q:22P I+H2@`CMJP&TPa:s2BH=>s#Q:N$% ix`qjzWdLHd+9?&eC');
define('NONCE_SALT',       '|Oqf<Sc!W^Ce?gIE9+Hzk]HJjA+5G]hW rXsZ2)[9[do 3;i<Z5<?]qS{5]!~A#h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
